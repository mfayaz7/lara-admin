<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Car extends Model
{
    use HasFactory,HasTranslations;

    protected $fillable = [
        'id','store_id','category_id', 'name','description','brand','model','color','type','age','kilometer','image','price'
    ];

    public $translatable = ['name','description','brand','model','color','type'];

    public function store(){
        return $this->belongsTo(Store::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
