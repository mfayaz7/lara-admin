<?php

namespace App\Http\Controllers;

use App\Models\RoleHasPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller
{
    public function index(Request $request)
    {
        Log::info('index permission');
        $query=RoleHasPermission::query();
        $query->with('roles')->with('permissions');

        $permissions =$query->paginate(5);
//        $role_permissions = Role::with('permissions')->get();
        return response()->json( $permissions,200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'permission_id' => 'required',
            'role_id'=>'required'
        ]);

        $role = Role::find($request->role_id);
        $permission = Permission::find($request->permission_id);
        $permission->assignRole($role);
        return response()->json( $permission,200);
    }

    public function deletePermissionRole(Request $request){
        Log::info('permission');

        $permission= Permission::findById($request->id);

        $role = Role::findByName($request->role);
        $role->revokePermissionTo($permission);
//        $permission->removeRole($request->role);
        if($permission->delete()){
            return response()->json([
            'message' => 'User delete successfully',
            'status_code' => 200
        ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
    public function getPermissionsWithRole(Request $request){
        Log::info('$permission');

        $query=Permission::query();
        $permissions=$query->whereHas('roles', function ($query) use ($request) {
            $query->where('name', '=',  $request->get('role'));
        })->get();
        $result=array();
        foreach($permissions as $permission):
            $result[]=$permission->name;
        endforeach;

        return response()->json( $result,200);
    }

    public function getPermissions(Request $request){
        Log::info('permission');
        $result=Permission::query()->get();

        return response()->json( $result,200);
    }

}
