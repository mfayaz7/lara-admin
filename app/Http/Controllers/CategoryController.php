<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('categoriess');
        $query=Category::query();

        if ($request->has('page')) {
            $categories = $query->orderBy('id', 'ASC')->paginate(5);
        }else{
            $categories = $query->orderBy('id', 'ASC')->get();
        }

        return response()->json( $categories,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:3|max:255',
            'desc_en' => 'required|min:1|max:255',
            'desc_ar' => 'required|min:1|max:255',
        ]);

        $category=new Category();
        $category->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
        $category->description = ['en' => $request->desc_en, 'ar' => $request->desc_ar];
        $category->status=true;
        $category->save();

        return response()->json( $category,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Category $category, Request $request)
    {
        Log::info('update.........');
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:3|max:255',
            'desc_en' => 'required|min:1|max:255',
            'desc_ar' => 'required|min:1|max:255',
        ]);

        $category->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
        $category->description = ['en' => $request->desc_en, 'ar' => $request->desc_ar];
        $category->status=true;
        $category->save();

        return response()->json( $category,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        Log::info('delete');
        if($category->delete()){
            return response()->json([
                'message' => 'Category delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
