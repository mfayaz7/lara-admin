<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $query=User::with('roles');
        if ($request->has('search')) {
            $query->where('name', 'like', '%' . $request->get('search') . '%')
            ->orWhere('email', 'like', '%' . $request->get('search') . '%')
            ->orWhere('phone', 'like', '%' . $request->get('search') . '%');
        }
        if ($request->has('filter')) {
            $query->whereHas('roles', function ($query) use ($request) {
                return $query->where('name', $request->get('filter'));
            });
        }
        if ($request->has('sort')){
            $users = $query->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $users = $query->orderBy('id' ,'ASC' )->paginate(5);

        return response()->json( $users,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'name' => 'required|min:3|max:255',
            'email' => 'required|unique:users|email|max:255',
            'password' => 'required|string|max:255|min:6',
            'role' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'password' => bcrypt($request->input('password')),
            ]);
        $role = Role::findByName($request->role);
        $user->assignRole($role);
        return response()->json( $user,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(User $user, Request $request)
    {
        Log::info('update.........');
        $validator = Validator::make($request->all(),[
            'name' => 'required|min:3|max:255',
            'email' => 'unique:users,email,'.$user->id,
            'role' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }

        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone')
        ]);

        $user->syncRoles($request->input('role'));
        return response()->json( $user,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(User $user)
    {
        Log::info('delete');
        if($user->delete()){
            return response()->json([
                'message' => 'User delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }

}
