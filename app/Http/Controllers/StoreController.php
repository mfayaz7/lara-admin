<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class StoreController extends Controller
{

    public function index(Request $request)
    {
        Log::info('stores');
//        $stores=Store::all();
        $query=Store::query();
        if ($request->has('search')) {
            $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%')
                ->orWhere('desc_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('desc_ar', 'like', '%' . $request->get('search') . '%');
        }

        if ($request->has('page')) {
            $stores = $query->orderBy('id','ASC')->paginate(5);
        }else
            $stores = $query->orderBy('id','ASC')->get()->all();

//        Log::info('size'.sizeof($tores));
        return response()->json( $stores,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'location_en' => 'required|min:1|max:255',
            'location_ar' => 'required|min:1|max:255',
            'password' => 'required|min:1|max:255',
            'email' => 'required|unique:users|email|max:255',
        ]);


        $password=bcrypt($request->password);


        $user =  new User();
        $user->name=$request->name_en;
        $user->password= $password;
        $user->email= $request->email;
        $user->phone= $request->phone;
        $user->save();

//        $role = Role::findByName('Admin');

        $adminRole = Role::whereName('Admin')->first();
        $user->assignRole($adminRole);

        $store=new Store();
        $store->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
        $store->location = ['en' => $request->location_en, 'ar' => $request->location_ar];
        $store->phone= $request->phone;
        $store->password= $password;
        $store->save();

        return response()->json( $store,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store $store
     * @return JsonResponse
     */
    public function update(Store $store, Request $request)
    {
        Log::info('update... store......');
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'location_en' => 'required|min:1|max:255',
            'location_ar' => 'required|min:1|max:255',
        ]);


        $store->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
        $store->location = ['en' => $request->location_en, 'ar' => $request->location_ar];
        $store->phone= $request->phone;
        $store->save();

        return response()->json( $store,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store $store
     * @return JsonResponse
     */
    public function destroy(Store $store)
    {
        Log::info('delete');
        if($store->delete()){
            return response()->json([
                'message' => 'Category delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
