<?php

namespace App\Http\Controllers;

use App\Models\Car;
use App\Models\Category;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('car');
        $query=Car::query();

            $cars = $query->orderBy('id','ASC')->paginate(5);

        return response()->json( $cars,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('storee');
        $request->validate([
            'store_id' => 'required',
            'category_id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'brand_en' => 'required',
            'brand_ar' => 'required',
            'model_en' => 'required',
            'model_ar' => 'required',
            'color_en' => 'required',
            'color_ar' => 'required',
            'type_en' => 'required',
            'type_ar' => 'required',
            'age' => 'required',
            'kilometer' => 'required',
            'image' => 'required',
            'price' => 'required',
        ]);

        $store=Store::find($request->store_id);
        $category=Category::find($request->category_id);

        $file = $request->file('image');
        $path = $file->hashName('car/folder');
        $image = Image::make($file);
        $image->fit(250, 250, function ($constraint) {
            $constraint->aspectRatio();
        });

        Storage::put($path, (string) $image->encode());

        Log::info('store image');

        $car=new Car();
        $car->store_id=$store->id;
        $car->category_id=$category->id;
        $car->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
        $car->description = ['en' => $request->description_en, 'ar' => $request->description_ar];
        $car->brand = ['en' => $request->brand_en, 'ar' => $request->brand_ar];
        $car->model = ['en' => $request->model_en, 'ar' => $request->model_ar];
        $car->color = ['en' => $request->color_en, 'ar' => $request->color_ar];
        $car->type = ['en' => $request->type_en, 'ar' => $request->type_ar];
        $car->age = $request->age;
        $car->kilometer = $request->kilometer;
        $car->image = $path;
        $car->price = $request->price;
        $car->save();
        Log::info('create ');

        return response()->json( $car,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Car  $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Car $car, Request $request)
    {
        Log::info('update.........');
        $request->validate([
            'store_id' => 'required',
            'category_id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'brand_en' => 'required',
            'brand_ar' => 'required',
            'model_en' => 'required',
            'model_ar' => 'required',
            'color_en' => 'required',
            'color_ar' => 'required',
            'type_en' => 'required',
            'type_ar' => 'required',
            'age' => 'required',
            'kilometer' => 'required',
            'price' => 'required',
        ]);


        $store=Store::find($request->store_id);
        $category=Category::find($request->category_id);

        $car->store_id=$store->id;
        $car->category_id=$category->id;
        $car->name = ['en' => $request->name_en, 'ar' => $request->name_ar];
        $car->description = ['en' => $request->description_en, 'ar' => $request->description_ar];
        $car->brand = ['en' => $request->brand_en, 'ar' => $request->brand_ar];
        $car->model = ['en' => $request->model_en, 'ar' => $request->model_ar];
        $car->color = ['en' => $request->color_en, 'ar' => $request->color_ar];
        $car->type = ['en' => $request->type_en, 'ar' => $request->type_ar];
        $car->age = $request->age;
        $car->kilometer = $request->kilometer;

        $oldPath=$car->image;

        if($request->hasFile('image')){
            Storage::delete($oldPath);
            $file = $request->file('image');
            $path = $file->hashName('car/folder');
            $image = Image::make($file);
            $image->fit(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($path, (string) $image->encode());
            $car->image=$path;

        }
//
        $car->price = $request->price;
//        $car->save();

        if($car->save())
        return response()->json( $car,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Car $car
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Car $car)
    {
        Log::info('delete');
        if($car->delete()){
            return response()->json([
                'message' => 'Category delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
