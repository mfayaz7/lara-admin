<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superAdminRole = Role::whereName('Super Admin')->first();
        $adminRole = Role::whereName('Admin')->first();
        $userRole = Role::whereName('User')->first();

        // Seed test admin
        $seededAdminEmail = 'webmaster@upbeat.com';
        $user = User::where('email', '=', $seededAdminEmail)->first();
        if ($user === null) {
            $user = User::create([
                'name' => 'superadmin',
                'email' => 'webmaster@upbeat.com',
                'phone' => '121564512',
                'password' =>bcrypt('123456'),
            ]);
            $user->assignRole($superAdminRole);
            $user->save();
        }

        $adminUser = User::where('email', '=', 'test_admin@upbeat.com')->first();
        if ($adminUser === null) {
            $adminUser = User::create([
                'name' => 'admin',
                'email' => 'test_admin@upbeat.com',
                'phone' => '564554665',
                'password' => bcrypt('123456'),
            ]);
            $adminUser->assignRole($adminRole);
            $adminUser->save();
        }



        // Seed test user
        $user = User::where('email', '=', 'testuser@upbeat.com')->first();
        if ($user === null) {
            $user = User::create([
                'name' => 'testuser',
                'email' => 'testuser@upbeat.com',
                'phone' => '54512151',
                'password' => bcrypt('123456'),
            ]);
            $user->assignRole($userRole);
            $user->save();
        }
    }
}
