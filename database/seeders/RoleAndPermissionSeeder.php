<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'edit store']);
        Permission::create(['name' => 'delete store']);
        Permission::create(['name' => 'update store']);
        Permission::create(['name' => 'read car']);
        Permission::create(['name' => 'read category']);
        Permission::create(['name' => 'all access']);

        // create roles and assign created permissions

        // this can be done as separate statements
        $role = Role::create(['name' => 'Admin']);
        $role->givePermissionTo('read car');
        $role->givePermissionTo('read category');

        $role = Role::create(['name' => 'Super Admin']);
        $role->givePermissionTo('all access');

        $role = Role::create(['name' => 'User']);
    }
}
