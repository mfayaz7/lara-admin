<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->foreignId('store_id')->constrained()->onDelete('cascade');
            $table->foreignId('category_id')->constrained()->onDelete('cascade');
            $table->string('name');
            $table->string('description');
            $table->string('image');
            $table->float('price');
            $table->string('brand');
            $table->string('model');
            $table->string('color');
            $table->string('type');
            $table->string('age');
            $table->string('kilometer');
            $table->integer('status')->default(1);
//            store_id(from store table),category_id (required ,from category table), product details like – name,
// description, image, price, brand, model, color, type, age, kilometer, status etc.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
