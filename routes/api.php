<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'],function (){
    Route::post('register',[AuthController::class,'register']);
    Route::post('login',[AuthController::class,'login']);

    Route::group(['middleware'=>'auth:api'],function (){
        Route::get('logout',[AuthController::class,'logout']);
    });
});



Route::group(['middleware' => 'auth:api'],function () {

    Route::get('/users', [UserController::class, 'index']);
    Route::post('users', [UserController::class, 'store']);
    Route::put('users/{id}', [UserController::class, 'update']);
    Route::delete('users/{user}', [UserController::class, 'destroy']);

    Route::get('/roles', [RoleController::class, 'index']);
    Route::post('roles', [RoleController::class, 'store']);
    Route::delete('roles/{role}', [RoleController::class, 'destroy']);

    Route::get('/permissions', [PermissionController::class, 'index']);
    Route::post('permissions', [PermissionController::class, 'store']);
    Route::post('/deletePermissionRole', [PermissionController::class, 'deletePermissionRole']);
    Route::get('/getPermissions', [PermissionController::class, 'getPermissionsWithRole']);
    Route::get('/permission', [PermissionController::class, 'getPermissions']);

    Route::get('/stores', [StoreController::class, 'index']);
    Route::post('stores', [StoreController::class, 'store']);
    Route::delete('stores/{store}', [StoreController::class, 'destroy']);
    Route::put('stores/{store}', [StoreController::class, 'update']);

    Route::get('/categories', [CategoryController::class, 'index']);
    Route::post('categories', [CategoryController::class, 'store']);
    Route::delete('categories/{category}', [CategoryController::class, 'destroy']);
    Route::put('categories/{category}', [CategoryController::class, 'update']);

    Route::get('/cars', [CarController::class, 'index']);
    Route::post('cars', [CarController::class, 'store']);
    Route::put('cars/{car}', [CarController::class, 'update']);
    Route::delete('cars/{car}', [CarController::class, 'destroy']);

});

