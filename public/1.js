(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Permission.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Permission.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_permission_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/permission_service */ "./resources/js/services/permission_service.js");
/* harmony import */ var _services_role_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/role_service */ "./resources/js/services/role_service.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Permission",
  data: function data() {
    return {
      permissionRoles: {},
      permissionData: [],
      permissions: [],
      roles: [],
      errors: {},
      page: 1
    };
  },
  created: function created() {
    this.getPermissions();
    this.getPermissionsRole(1);
    this.getRoles();
  },
  methods: {
    hideNewPermissionModel: function hideNewPermissionModel() {
      this.$refs.newPermissionModel.hide();
    },
    showNewPermissionModel: function showNewPermissionModel() {
      this.$refs.newPermissionModel.show();
    },
    getRoles: function () {
      var _getRoles = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _services_role_service__WEBPACK_IMPORTED_MODULE_2__["getRoles"]();

              case 3:
                response = _context.sent;
                this.roles = response.data;
                _context.next = 10;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 7]]);
      }));

      function getRoles() {
        return _getRoles.apply(this, arguments);
      }

      return getRoles;
    }(),
    getPermissions: function () {
      var _getPermissions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _services_permission_service__WEBPACK_IMPORTED_MODULE_1__["getPermissions"]();

              case 3:
                response = _context2.sent;
                console.log(response.data);
                this.permissions = response.data;
                _context2.next = 11;
                break;

              case 8:
                _context2.prev = 8;
                _context2.t0 = _context2["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 8]]);
      }));

      function getPermissions() {
        return _getPermissions.apply(this, arguments);
      }

      return getPermissions;
    }(),
    getPermissionsRole: function () {
      var _getPermissionsRole = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var page,
            params,
            response,
            _args3 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                page = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : 1;
                params = {
                  pages: page
                };
                _context3.prev = 2;
                console.log('permission');
                _context3.next = 6;
                return _services_permission_service__WEBPACK_IMPORTED_MODULE_1__["getPermissionsRole"](params);

              case 6:
                response = _context3.sent;
                console.log(response.data);
                this.permissionRoles = response.data;
                _context3.next = 14;
                break;

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](2);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[2, 11]]);
      }));

      function getPermissionsRole() {
        return _getPermissionsRole.apply(this, arguments);
      }

      return getPermissionsRole;
    }(),
    createPermission: function () {
      var _createPermission = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                formData = new FormData();
                formData.append('permission_id', this.permissionData.permission);
                formData.append('role_id', this.permissionData.role);
                _context4.prev = 3;
                _context4.next = 6;
                return _services_permission_service__WEBPACK_IMPORTED_MODULE_1__["createPermission"](formData);

              case 6:
                response = _context4.sent;
                this.errors = {};
                this.permissions.unshift(response.data);
                this.hideNewPermissionModel();
                this.flashMessage.success({
                  title: 'Permission Successfully Created'
                });
                _context4.next = 23;
                break;

              case 13:
                _context4.prev = 13;
                _context4.t0 = _context4["catch"](3);
                _context4.t1 = _context4.t0.response.status;
                _context4.next = _context4.t1 === 422 ? 18 : _context4.t1 === 500 ? 20 : 22;
                break;

              case 18:
                this.flashMessage.error({
                  message: _context4.t0.response.data.errors,
                  time: 5000
                });
                return _context4.abrupt("break", 23);

              case 20:
                this.flashMessage.error({
                  message: _context4.t0.response.data.message,
                  time: 5000
                });
                return _context4.abrupt("break", 23);

              case 22:
                this.flashMessage.error({
                  message: _context4.t0.response.data.message,
                  time: 5000
                });

              case 23:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[3, 13]]);
      }));

      function createPermission() {
        return _createPermission.apply(this, arguments);
      }

      return createPermission;
    }(),
    deletePermission: function () {
      var _deletePermission = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5(permission) {
        var _this = this;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                console.log('delete');
                this.$confirm({
                  title: 'Are you sure?',
                  message: "You want to delete ".concat(permission.name, " ?"),
                  button: {
                    no: 'No',
                    yes: 'Yes'
                  },

                  /**
                   * Callback Function
                   * @param {Boolean} confirm
                   */
                  callback: function callback(confirm) {
                    if (confirm) {
                      try {
                        console.log(permission.roles[0].name);
                        var formData = new FormData();
                        formData.append('id', permission.id);
                        formData.append('role', permission.roles[0].name);
                        _services_permission_service__WEBPACK_IMPORTED_MODULE_1__["deletePermissionWithRole"](formData);
                        _this.permissions = _this.permissions.filter(function (obj) {
                          return obj.id != permission.id;
                        });
                      } catch (error) {
                        _this.flashMessage.error({
                          message: error.response.data.message,
                          time: 5000
                        });
                      }
                    }
                  }
                });

              case 2:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function deletePermission(_x) {
        return _deletePermission.apply(this, arguments);
      }

      return deletePermission;
    }()
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xl-12 col-lg-12" },
        [
          _c(
            "div",
            {
              staticClass:
                "card-header py-3 d-flex flex-row align-items-center justify-content-between"
            },
            [
              _c("h6", { staticClass: "m-0 font-weight-bold text-primary" }, [
                _vm._v("Create Permission")
              ]),
              _vm._v(" "),
              _c(
                "b-button",
                {
                  attrs: { id: "show-btn", variant: "primary" },
                  on: { click: _vm.showNewPermissionModel }
                },
                [_vm._v("Create Permission")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "newPermissionModel",
              attrs: { "hide-footer": "", title: "create new role" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.createPermission.apply(null, arguments)
                    }
                  }
                },
                [
                  _c("form", [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "permission_name" } }, [
                        _vm._v("Permission Name")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.permissionData.permission,
                              expression: "permissionData.permission"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "permission_name" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.permissionData,
                                "permission",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.permissions, function(permission) {
                          return _c(
                            "option",
                            { domProps: { value: permission.id } },
                            [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(permission.name) +
                                  "\n                                "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "role" } }, [_vm._v("Role")]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.permissionData.role,
                              expression: "permissionData.role"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "role" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.permissionData,
                                "role",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.roles, function(role) {
                          return _c(
                            "option",
                            { domProps: { value: role.id } },
                            [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(role.name) +
                                  "\n                                "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-success",
                        attrs: { type: "submit" }
                      },
                      [_vm._v("save")]
                    )
                  ])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "card shadow mb-4" }, [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass: "table table-bordered",
                    attrs: { id: "dataTable", width: "100%", cellspacing: "0" }
                  },
                  [
                    _vm._m(1),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(_vm.permissionRoles.data, function(
                        permission,
                        index
                      ) {
                        return _vm.permissionRoles.data
                          ? _c("tr", { key: index }, [
                              _c("td", [_vm._v(_vm._s(index + 1))]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(_vm._s(permission.permissions.name))
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(permission.roles.name))]),
                              _vm._v(" "),
                              _c("td", [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-danger btn-circle btn-sm",
                                    on: {
                                      click: function($event) {
                                        return _vm.deletePermission(permission)
                                      }
                                    }
                                  },
                                  [_c("i", { staticClass: "fas fa-trash" })]
                                )
                              ])
                            ])
                          : _vm._e()
                      }),
                      0
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "mt-3 text-center" },
                  [
                    _c("pagination", {
                      attrs: { data: _vm.permissionRoles },
                      on: { "pagination-change-page": _vm.getPermissionsRole }
                    })
                  ],
                  1
                )
              ])
            ])
          ])
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "d-sm-flex align-items-center justify-content-between mb-4"
      },
      [
        _c("h1", { staticClass: "h3 mb-0 text-gray-800" }, [
          _vm._v("Permissions")
        ])
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [_vm._v("S.no")]),
        _vm._v(" "),
        _c("th", [_vm._v("Permissions")]),
        _vm._v(" "),
        _c("th", [_vm._v("Roles")]),
        _vm._v(" "),
        _c("th", [_vm._v("Action")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/services/permission_service.js":
/*!*****************************************************!*\
  !*** ./resources/js/services/permission_service.js ***!
  \*****************************************************/
/*! exports provided: createPermission, getPermissions, getPermissionsRole, deletePermission, deletePermissionWithRole */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createPermission", function() { return createPermission; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPermissions", function() { return getPermissions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPermissionsRole", function() { return getPermissionsRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deletePermission", function() { return deletePermission; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deletePermissionWithRole", function() { return deletePermissionWithRole; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createPermission(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/permissions', data);
}
function getPermissions() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/permission');
}
function getPermissionsRole(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/permissions', params);
}
function deletePermission(id, role) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/permissions/".concat(id));
}
function deletePermissionWithRole(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/deletePermissionRole', params);
}

/***/ }),

/***/ "./resources/js/services/role_service.js":
/*!***********************************************!*\
  !*** ./resources/js/services/role_service.js ***!
  \***********************************************/
/*! exports provided: createRole, getRoles, deleteRole */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createRole", function() { return createRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRoles", function() { return getRoles; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteRole", function() { return deleteRole; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createRole(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/roles', data);
}
function getRoles(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/roles', {
    params: params
  });
}
function deleteRole(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/roles/".concat(id));
}

/***/ }),

/***/ "./resources/js/views/Permission.vue":
/*!*******************************************!*\
  !*** ./resources/js/views/Permission.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Permission_vue_vue_type_template_id_5f5bf6a4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true& */ "./resources/js/views/Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true&");
/* harmony import */ var _Permission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Permission.vue?vue&type=script&lang=js& */ "./resources/js/views/Permission.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Permission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Permission_vue_vue_type_template_id_5f5bf6a4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Permission_vue_vue_type_template_id_5f5bf6a4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "5f5bf6a4",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Permission.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Permission.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/views/Permission.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Permission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Permission.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Permission.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Permission_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Permission_vue_vue_type_template_id_5f5bf6a4_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Permission.vue?vue&type=template&id=5f5bf6a4&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Permission_vue_vue_type_template_id_5f5bf6a4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Permission_vue_vue_type_template_id_5f5bf6a4_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);