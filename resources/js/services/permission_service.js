import {http} from './http_service'

export function createPermission(data) {
    return http().post('/permissions',data);
}

export function getPermissions() {
    return http().get('/permission');
}
export function getPermissionsRole(params) {
    return http().get('/permissions',params);
}

export function deletePermission(id,role) {
    return http().delete(`/permissions/${id}`);
}
export function deletePermissionWithRole(params) {
    return http().post('/deletePermissionRole' ,params);
}

