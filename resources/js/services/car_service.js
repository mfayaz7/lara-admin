import {http,httpFile} from './http_service'

export function createCar(data) {
    return httpFile().post('/cars',data);
}

export function deleteCar(id) {
    return http().delete(`/cars/${id}`);
}

export function updateCar(id,data) {
    return httpFile().post(`/cars/${id}`,data);
}

export function getCar(params) {
    return http().get('/cars' ,  {params:params});
}

