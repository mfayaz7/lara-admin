import {http} from './http_service'

export function createRole(data) {
    return http().post('/roles',data);
}

export function getRoles(params) {
    return http().get('/roles',{params:params});
}

export function deleteRole(id) {
    return http().delete(`/roles/${id}`);
}
