import {http} from './http_service'

export function createStore(data) {
    return http().post('/stores',data);
}

export function deleteStore(id) {
    return http().delete(`/stores/${id}`);
}

export function updateStore(id,data) {
    return http().post(`/stores/${id}`,data);
}

export function getStore(params) {
    return http().get('/stores', {params: params});
}
export function getStores() {
    return http().get('/stores');
}

