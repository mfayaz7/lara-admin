import {http} from './http_service'

export function createCategory(data) {
    return http().post('/categories',data);
}

export function deleteCategory(id) {
    return http().delete(`/categories/${id}`);
}

export function updateCategory(id,data) {
    return http().post(`/categories/${id}`,data);
}

export function getCategory(params) {
    return http().get('/categories', {params: params});
}
export function getCategories() {
    return http().get('/categories');
}

